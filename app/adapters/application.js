import DS from "ember-data";
import ENV from "../config/environment";

export default DS.ActiveModelAdapter.extend({
  namespace: ENV.END_POINT.namespace,
  host: ENV.END_POINT.host,
  headers: function(){
    var headers = this.get("session.headers");
    console.log("injecting headers:", headers);
    return headers;
  }.property("session.headers"),
  ajaxError: function(jqXHR, responseText){
    if(jqXHR && jqXHR.status===401)
    {
      this.get("session").signOut();
    }
    return this._super(jqXHR, responseText);
  }
});
