import DS from 'ember-data';
/* global moment */

export default DS.Model.extend({
  date: DS.attr( 'string' ),
  time: DS.attr( 'string' ),
  amount: DS.attr( 'string' ),
  amountStr: function(){
    var amount = this.get("amount");
    return "$" + parseFloat(amount).toFixed(2);
  }.property("amount"),
  description: DS.attr( 'string' ),
  comment: DS.attr( 'string' ),
  matchSearch: function(regexp){
    return this.get("date").match(regexp) ||
      this.get("time").match(regexp) ||
      this.get("amount").match(regexp) ||
      this.get("description").match(regexp) ||
      this.get("comment").match(regexp);
  },
  week: function(){
    var date = moment(this.get("date"));
    return "From " + date.startOf('week').format("MMMM Do YYYY") +
      " to " + date.endOf('week').format("MMMM Do YYYY");
  }.property("date"),
});
