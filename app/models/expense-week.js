import Ember from "ember";

export default Ember.ArrayProxy.extend({
  totalAmount: function(){
    var content = this.get("content");
    return content.reduce(
      function(previousValue, item){
        return previousValue+parseFloat(item.get("amount"));
      },
      0
    );
  }.property("content.@each", "content.@each.amount"),
  totalAmountStr: function(){
    var total_amount = this.get("totalAmount");
    return "$" + total_amount.toFixed(2);

  }.property("totalAmount"),
  averageDaySpending: function(){
    var total_amount = this.get("totalAmount");
    return "$" + (total_amount/7).toFixed(2);
  }.property("totalAmount")
});
