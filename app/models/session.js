import Ember from "ember";

export default Ember.Object.extend({
  auth_token: null, //"0a0418b8-83c2-49aa-b94a-ceabe01f7474",
  account_id: null, //"1",
  isAuthenticated: function(){
    return this.get("auth_token")!=null;
  }.property("auth_token", "account_id"),
  headers: function(){
    if(this.get("isAuthenticated"))
    {
      return {auth_token: this.get("auth_token")};
    }
    else
    {
      return {};
    }
  }.property("auth_token"),
  signOut: function(){
    console.log("sign out!");
    this.setProperties({auth_token: null, account_id: null});
  }
});
