import Ember from "ember";

export default Ember.Component.extend({
  didInsertElement: function(){
    var element = this.$();
    // Focus state for append/prepend inputs
    element.on("focus", ".form-control", function(){
      element.closest(".input-group, .form-group").addClass("focus");
    }).on("blur", ".form-control", function(){
      element.closest(".input-group, .form-group").removeClass("focus");
    });
  }
});


