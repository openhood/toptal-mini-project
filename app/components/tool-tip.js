import Ember from "ember";

export default Ember.Component.extend({
  tagName: "span",
  classNames: ["tool-tip"],
  didInsertElement: function(){
    var element = this.$();
    element.tooltip({
      placement: "left",
      title: this.get("title"),
      container: "body"
    });
  }
});


