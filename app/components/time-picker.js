import Ember from "ember";

export default Ember.Component.extend({
  didInsertElement: function(){
    var control = this.$().find(".form-control");
    control.timepicker({
      forceRoundTime: true,
      noneOption: true,
      scrollDefault: "now",
      step: 15,
      timeFormat: "H:i",
      className: "timepicker-primary"
    });
  },
  willDestroyElement: function(){
    var control = this.$().find(".form-control");
    control.timepicker("remove");
  }
});
