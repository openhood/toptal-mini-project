import Ember from "ember";

export default Ember.Component.extend({
  tagName: "a",
  attributeBindings: ["href"],
  href: "#",
  didInsertElement: function(){
    var target = Ember.$("#" + this.get("target"));
    this.$().on("click", function(e){
      e.stopPropagation();
      target.modal();
      return false;
    });
  }
});


