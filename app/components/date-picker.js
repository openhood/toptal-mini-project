import Ember from "ember";

export default Ember.Component.extend({
  didInsertElement: function(){
    var control = this.$().find(".form-control");
    control.datepicker({
      showOtherMonths: true,
      selectOtherMonths: true,
      dateFormat: "yy-mm-dd",
      yearRange: "-1:+1"
    }).prev(".input-group-btn").on("click", function(e){
      e.preventDefault();
      control.focus();
    });
    Ember.$.extend(Ember.$.datepicker, {
      _checkOffset: function(inst, offset){
        return offset;
      }
    });
    control.datepicker("widget").css({
      "margin-left": 3-control.prev(".input-group-btn").find(".btn").outerWidth()
    });
  },
  willDestroyElement: function(){
    var control = this.$().find(".form-control");
    control.datepicker("destroy");
  }
});
