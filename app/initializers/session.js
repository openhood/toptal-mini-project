import Session from "../models/session";

export function initialize(container, application) {
  application.register("session:main", Session);
  application.inject("route",        "session", "session:main");
  application.inject("controller",   "session", "session:main");
  application.inject("adapter",      "session", "session:main");
}

export default {
  name: 'session',
  initialize: initialize
};
