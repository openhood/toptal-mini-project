import Ember from 'ember';

export default Ember.Route.extend({
  setupController: function(controller, model){
    this._super(controller, model);
    controller.set("newUser", this.store.createRecord("user"));
  },
  beforeModel: function(){
    var session = this.get("session");
    console.log("before model sign-in:", session);
    if(session.get("isAuthenticated"))
    {
      this.transitionTo("expenses");
    }
  }
});
