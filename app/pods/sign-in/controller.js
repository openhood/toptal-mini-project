import Ember from 'ember';

export default Ember.ArrayController.extend({
  unauthorized: false,
  newUserCreated: false,
  actions: {
    create: function(){
      var self = this;
      var adapter = self.store.adapterFor("session");
      adapter.ajax(
        adapter.buildURL("session"),
        "POST",
        {
          data: {
            username: self.get("username"),
            password: self.get("password")
          }
        }
      ).then(
        function(response){ // success
          self.get("session").setProperties(response.session);
          self.set("unauthorized", false);
          self.set("username", null);
          self.set("password", null);
          self.transitionToRoute("expenses");
        },
        function(){ // failure
          console.log("failure");
          self.set("unauthorized", true);
          self.set("password", null);
        }
      );
    },
    createUser: function(){
      var self = this;
      var newUser = self.get("newUser");
      newUser.save().then(
        function(){ // success
          self.set("newUser", self.store.createRecord("user"));
          self.set("newUserCreated", true);
        },
        function(){ // failure
        }
      );
    }
  }
});
