import Ember from 'ember';
import ExpenseWeek from "../../../models/expense-week";

export default Ember.ArrayController.extend({
  commitedExpenses: function(){
    var search = this.get("search");
    var filtered = this.get("arrangedContent").filterBy("isNew", false);
    if(search){
      var regexp = new RegExp(search, "gi");
      filtered = filtered.filter(function(expense){
        return expense.matchSearch(regexp);
      });
    }
    var grouped = Ember.A([]);
    filtered.forEach(function(expense){
      var key = expense.get("week");
      var found = grouped.findProperty("group", key);
      if(!found){
        found = grouped.pushObject(
          ExpenseWeek.create({group: key, content: Ember.A([])})
        );
      }
      found.get("content").pushObject(expense);
    });
    return grouped;
  }.property("arrangedContent.@each", "search"),
  search: null,
  sortProperties: ["date", "time"],
  sortAscending: false,
  actions: {
    create: function(){
      var self = this;
      var newExpense = self.get("newExpense");
      newExpense.save().then(
        function(){ // success
          self.set("newExpense", self.store.createRecord("expense"));
        },
        function(){ // failure
        }
      );
    }
  }
});
