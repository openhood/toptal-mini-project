import ApplicationRoute from '../../../routes/application';

export default ApplicationRoute.extend({
  setupController: function(controller, model){
    this._super(controller, model);
    controller.set("newExpense", this.store.createRecord("expense"));
  },
  model: function(){
    return this.store.find("expense");
  }
});
