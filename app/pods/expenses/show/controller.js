import Ember from 'ember';

export default Ember.ObjectController.extend({
  isEditing: false,
  isDeleting: false,
  actions: {
    edit: function(){
      console.log("edit");
      this.set("isEditing", true);
    },
    cancel: function(){
      console.log("cancel");
      this.get("model").rollback();
      this.set("isEditing", false);
    },
    remove: function(){
      console.log("remove");
      var self = this;
      self.set("isDeleting", true);
      var model = self.get("model");
      var adapter = self.store.adapterFor("expense");
      adapter.ajax(
        adapter.buildURL("expense", model.get("id"), model),
        "DELETE"
      ).then(
        function(){ // success
          console.log("success");
          self.store.unloadRecord(model);
        },
        function(){ // failure
          console.log("failure");
          self.set("isDeleting", false);
        }
      );
    },
    save: function(){
      console.log("save");
      var self = this;
      self.get("model").save().then(
        function(){ // success
          self.set("isEditing", false);
        },
        function(){ // failure
        }
      );

    }
  }
});
