import Ember from 'ember';

export default Ember.Route.extend({
  beforeModel: function(){
    var session = this.get("session");
    if(!session.get("isAuthenticated"))
    {
      this.transitionTo("sign-in");
    }
  },
  signedOut: function(){
    if(!this.get("session.isAuthenticated"))
    {
      this.transitionTo("sign-in");
    }
  }.observes("session.isAuthenticated"),
  actions: {
    destroySession: function(){
      this.get("session").signOut();
    }
  }
});
