# Write a simple expenses tracker web application

The user must be able to:
[√] log in
[√] when logged in, user can see, edit and delete expenses he entered
[√] when entered, each expense has: date, time, description, amount, comment
[√] minimal UI/UX design is needed (you will not be marked on graphic design, however, do try to keep it as tidy as possible)
[√] every client operation done using JavaScript, reloading the page is not an option
[√] REST API
[√] make it possible to perform all user actions via the API
[√] including authentication
[√] you need to be able to pass credentials to both the webpage and the API
[√] be able to explain how a REST API works
[√] create functional tests that use the REST layer directly
[1] bonus: unit tests!
[√] create an account
[√] user can filter expenses
[√] user can print expenses
[√]   per week
[√]   with total amount and average day spending

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM) and [Bower](http://bower.io/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember server`
* Visit your app at http://localhost:4200.
