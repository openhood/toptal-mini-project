/* global require, module */

var EmberApp = require('ember-cli/lib/broccoli/ember-app');

var app = new EmberApp();

var index = app.legacyFilesToAppend.indexOf('bower_components/handlebars/handlebars.runtime.js');
if(index) {
    app.legacyFilesToAppend[index] = 'bower_components/handlebars/handlebars.js';
}

// Use `app.import` to add additional libraries to the generated
// output files.
//
// If you need to use different assets in different
// environments, specify an object as the first parameter. That
// object's keys should be the environment name and the values
// should be the asset to use in that environment.
//
// If the library that you are including contains AMD or ES6
// modules that you would like to import into your application
// please specify an object with the list of modules as keys
// along with the exports of each module as its value.

app.import('bower_components/moment/moment.js');
app.import('vendor/flat-ui-pro/css/vendor/bootstrap.min.css');
app.import('vendor/flat-ui-pro/css/flat-ui-pro.min.css');
app.import('vendor/flat-ui-pro/js/flat-ui-pro.js');

var mergeTrees = require('broccoli-merge-trees');
var pickFiles = require('broccoli-static-compiler');
var fontAssets = pickFiles('vendor/flat-ui-pro/fonts', {
  srcDir: '/',
  files: ['**/*.eot', '**/*.svg', '**/*.ttf', '**/*.woff'],
  destDir: '/fonts'
});
var imgAssets = pickFiles('vendor/flat-ui-pro/img', {
  srcDir: '/',
  files: ['**/*.png', '**/*.svg', '**/*.ico'],
  destDir: '/img'
});
module.exports = mergeTrees([app.toTree(), fontAssets, imgAssets]);
