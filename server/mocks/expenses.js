module.exports = function(app) {
  var express = require("express");
  var db = require("../helpers/db");
  var Utils = require("../helpers/utils");

  var expensesRouter = express.Router();
  expensesRouter.use(
    function(req, res, next){
      db.all(
        "SELECT user_id FROM sessions WHERE auth_token=? LIMIT 1",
        req.header("auth_token"),
        function(err, rows){
          if(err){
            console.log(err);
            res.status(500).end();
          } else if(rows.length>0) {
            req.user_id = rows[0].user_id;
            next();
          } else {
            res.status(401).end();
          }
        }
      );
    }
  );
  expensesRouter.get("/", function(req, res){
    db.all(
      "SELECT id, date, time, amount, description, comment FROM expenses WHERE user_id=?",
      req.user_id,
      function(err, rows){
        if(err){
          console.log(err);
          res.status(500).end();
        } else {
          res.send({"expenses": rows});
        }
      }
    );
  });
  expensesRouter.post("/", function(req, res) {
    var expense = Utils.validateExpense(req.body.expense);
    if(Object.keys(expense.errors).length>0){
      res.status(422).send({"errors": expense.errors});
    } else {
      db.run(
        "INSERT INTO expenses\
          (user_id, date, time, amount, description, comment)\
          VALUES (?, ?, ?, ?, ?, ?)",
        req.user_id,
        expense.date,
        expense.time,
        expense.amount,
        expense.description,
        expense.comment,
        function(err, row){
          if(err){
            console.log(err);
            res.status(500).end();
          } else {
            expense.id = this.lastID;
            res.send({"expense": expense});
          }
        }
      );
    }
  });
  expensesRouter.delete("/:id", function(req, res) {
    db.run(
      "DELETE FROM expenses WHERE id=? AND user_id=?",
      req.params.id,
      req.user_id,
      function(err, rows){
        if(err){
          console.log(err);
          res.status(500).end();
        } else if(this.changes>0) {
          res.status(204).end();
        } else {
          console.log(this);
          res.status(404).end();
        }
      }
    )
  });
  expensesRouter.put("/:id", function(req, res) {
    var id = req.params.id;
    db.all(
      "SELECT * FROM expenses WHERE id=? AND user_id=? LIMIT 1",
      id,
      req.user_id,
      function(err, rows){
        if(err){
          console.log(err);
          res.status(500).end();
        } else if(rows.length>0) {
          var expense = Utils.validateExpense(req.body.expense);
          if(Object.keys(expense.errors).length>0){
            res.status(422).send({"errors": expense.errors});
          } else {
            db.run(
              "UPDATE expenses SET\
                date=?, time=?, amount=?, description=?, comment=?\
                WHERE id=?",
              expense.date,
              expense.time,
              expense.amount,
              expense.description,
              expense.comment,
              id,
              function(err, row){
                if(err){
                  console.log(err);
                  res.status(500).end();
                } else {
                  res.status(204).end();
                }
              }
            );
          }
        } else {
          res.status(404).end();
        }
      }
    )
  });
  app.use("/api/expenses", expensesRouter);
};
