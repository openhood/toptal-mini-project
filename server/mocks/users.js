module.exports = function(app) {
  var express = require("express");
  var bcrypt = require("bcrypt");
  var db = require("../helpers/db");
  var Utils = require("../helpers/utils");

  var usersRouter = express.Router();
  usersRouter.post("/", function(req, res) {
    var user = req.body.user || {};
    var errors = {}
    var email = Utils.cleanStr(user.email);
    if((email.match(/@/g) || []).length!=1){
      errors["email"] = ["is not valid"]
    }
    var password = Utils.cleanStr(user.password);
    if(password==""){
      errors["password"] = ["is mandatory"]
    }
    if(Object.keys(errors).length>0){
      res.status(422).send({"errors": errors});
    } else {
      var crypted_password = bcrypt.hashSync(password, 10);
      db.run(
        "INSERT INTO users(email, crypted_password) VALUES (?, ?)",
        email,
        crypted_password,
        function(err, row){
          if(err && err.code=="SQLITE_CONSTRAINT"){
            errors["email"] = ["is already taken"];
            res.status(422).send({"errors": errors});
          } else if(err) {
            console.log(err);
            res.status(500).end();
          } else {
            user.id = this.lastID;
            delete user.password;
            res.send({"user": user});
          }
        }
      );
    }
  });
  app.use("/api/users", usersRouter);
};
