module.exports = function(app) {
  var express = require("express");
  var bcrypt = require("bcrypt");
  var db = require("../helpers/db");
  var Utils = require("../helpers/utils");

  var sessionsRouter = express.Router();
  sessionsRouter.post("/", function(req, res){
    var username = Utils.cleanStr(req.body.username);
    var password = Utils.cleanStr(req.body.password);
    db.all(
      "SELECT * FROM users WHERE email=? LIMIT 1",
      username,
      function(err, rows){
        if(err){
          console.log(err);
          res.status(500).end();
        } else if(rows.length>0) {
          var user = rows[0];
          bcrypt.compare(
            password,
            user.crypted_password,
            function(err, match){
              if(match===true){
                var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(
                  /[xy]/g,
                  function(c){
                    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                    return v.toString(16);
                  }
                );
                db.run(
                  "INSERT INTO sessions(user_id, auth_token) VALUES (?, ?)",
                  user.id,
                  uuid,
                  function(err, row){
                    if(err && err.code=="SQLITE_CONSTRAINT"){
                      res.status(202).end(); // try later
                    } else if(err) {
                      console.log(err);
                      res.status(500).end();
                    } else {
                      res.send({session: {account_id: user.id, auth_token: uuid}});
                    }
                  }
                );
              } else {
                res.status(401).end(); // password incorrect
              }
            }
          );
        } else {
          res.status(401).end(); // username doesn't exist
        }
      }
    );
  });
  sessionsRouter.delete("/:auth_token", function(req, res){
    var auth_token = req.params.auth_token;
    db.run(
      "DELETE FROM sessions WHERE auth_token=?",
      auth_token,
      function(err, row){
        if(err){
          console.log(err);
          res.status(500).end();
        } else if(this.changes>0) {
          res.status(204).end();
        } else {
          res.status(404).end();
        }
      }
    );
  });
  app.use("/api/sessions", sessionsRouter);
};
