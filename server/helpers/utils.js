var XRegExp = require("xregexp").XRegExp;
var moment = require("moment");
var Utils = {
  replace: function(input, search, replacement){
    var regexp = XRegExp(search, "g");
    return XRegExp.replace(input, regexp, replacement);
  },
  cleanStr: function(input){
    // http://xregexp.com/addons/unicode/unicode-categories.js
    // http://blog.nodejs.org/2014/06/16/openssl-and-breaking-utf-8-change/
    // \\p{C} = control characters
    // \ufffd = unknown unicode character (U+FFFD)
    // \\p{Z} = invisible characters
    var output = input || "";
    output = this.replace(output, "[\\p{C}\ufffd]", "");
    output = this.replace(output, "\\p{Z}", " ");
    return output.trim();
  },
  cleanDate: function(input){
    return this.cleanMoment(input, "YYYY-MM-DD");
  },
  cleanTime: function(input){
    return this.cleanMoment(input, "HH:mm");
  },
  cleanMoment: function(input, format){
    var m = moment(this.cleanStr(input), format);
    if(m.isValid()){
      return m.format(format);
    } else {
      return undefined;
    }
  },
  validateExpense: function(input){
    input = input || {};
    var output = {errors: {}}
    var date = Utils.cleanStr(input.date);
    if(date==""){
      output.errors.date = ["can't be blank"];
    } else {
      date = Utils.cleanDate(date);
      if(date){
        output.date = date;
      } else {
        output.errors.date = ["format invalid, please use YYYY-MM-DD"];
      }
    }
    var time = Utils.cleanStr(input.time);
    if(time!=""){
      time = Utils.cleanTime(time);
      if(time){
        output.time = time;
      } else {
        output.errors.time = ["format invalid, please use HH:mm"];
      }
    }
    var amount = Utils.cleanStr(input.amount);
    if(amount==""){
      output.errors.amount = ["can't be blank"];
    } else {
      amount = parseFloat(amount);
      if(isNaN(amount)){
        output.errors.amount = ["format invalid, example: 32.10"];
      } else {
        output.amount = amount.toFixed(2);
      }
    }
    output.description = Utils.cleanStr(input.description);
    output.comment = Utils.cleanStr(input.comment);
    return output;
  }
};
module.exports = Utils;
