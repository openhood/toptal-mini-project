var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(":memory:");
var bcrypt = require("bcrypt");
db.serialize(function() {
  db.run("CREATE TABLE IF NOT EXISTS users(\
    id INTEGER PRIMARY KEY,\
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,\
    email TEXT,\
    crypted_password TEXT\
  )");
  db.run("CREATE UNIQUE INDEX IF NOT EXISTS users_email_unique ON users(email)");
  db.run("CREATE TABLE IF NOT EXISTS sessions(\
    id INTEGER PRIMARY KEY,\
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,\
    user_id INTEGER,\
    auth_token TEXT NOT NULL,\
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE\
  )");
  db.run("CREATE UNIQUE INDEX IF NOT EXISTS sessions_auth_token_unique ON sessions(auth_token)");
  db.run("CREATE TABLE IF NOT EXISTS expenses(\
    id INTEGER PRIMARY KEY,\
    created_at TIMESTAMP NOT NULL DEFAULT current_timestamp,\
    user_id INTEGER,\
    date TEXT,\
    time TEXT,\
    amount DECIMAL(16,4),\
    description TEXT,\
    comment TEXT,\
    FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE\
  )");
  db.run("CREATE INDEX IF NOT EXISTS expenses_user_id ON expenses(user_id)");
  db.run(
    "INSERT INTO users(email, crypted_password) VALUES (?, ?)",
    "top",
    bcrypt.hashSync("top", 10),
    function(err, row)
    {
      if(err)
      {
        console.log(err);
      }
      else{
        var user_id = this.lastID;
        var stmt = db.prepare("INSERT INTO expenses(user_id, date, time, amount, description, comment) VALUES (?, ?, ?, ?, ?, ?)");
        stmt.run(user_id, "2014-01-10", "12:00", "15.00", "Coffee", "Black");
        stmt.run(user_id, "2014-01-14", "10:00", "10.00", "Coffee cups", "Complimentary");
        stmt.finalize();
      }
    }
  );
});
module.exports = db;
