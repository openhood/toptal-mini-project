import Ember from 'ember';
import startApp from '../helpers/start-app';
import utils from '../helpers/utils';

var App;

module("API: sessions resource", {
  setup: function() {
    App = startApp();
  },
  teardown: function() {
    Ember.run(App, 'destroy');
  }
});

asyncTest("POST /api/sessions with valid username and password returns 200", function(){
  expect(3);
  utils.generateUser(
    function(user){
      $.post(
        "/api/sessions",
        {username: user.email, password: "sponge"}
      ).then(
        function(data_session){ // success
          var session = data_session.session;
          equal(session.account_id, user.id);
          ok(session.auth_token!=null);
          start();
        },
        function(){ // failure
          start();
        }
      );
    }
  );
});

asyncTest("POST /api/sessions with invalid username returns 401", function(){
  expect(2);
  utils.generateUser(
    function(user){
      $.post(
        "/api/sessions",
        {username: utils.generateEmail(), password: "sponge"}
      ).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 401);
          start();
        }
      );
    }
  );
});

asyncTest("POST /api/sessions with valid username but wrong password returns 401", function(){
  expect(2);
  utils.generateUser(
    function(user){
      $.post(
        "/api/sessions",
        {username: user.email, password: "wrong"}
      ).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 401);
          start();
        }
      );
    }
  );
});

asyncTest("POST /api/sessions with no data at all returns 401", function(){
  expect(2);
  utils.generateUser(
    function(user){
      $.post("/api/sessions").then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 401);
          start();
        }
      );
    }
  );
});

asyncTest("GET /api/sessions returns 404", function(){
  expect(1);
  $.get("/api/sessions").then(
    function(){ // success
      start();
    },
    function(data){ // failure
      equal(data.status, 404);
      start();
    }
  );
});

asyncTest("GET /api/sessions/:auth_token returns 404", function(){
  expect(3);
  utils.generateSession(
    function(session){
      $.get("/api/sessions/" + session.auth_token).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 404);
          start();
        }
      );
    }
  );
});

asyncTest("PUT /api/sessions/:auth_token returns 404", function(){
  expect(3);
  utils.generateSession(
    function(session){
      $.ajax({
        type: "PUT",
        url: "/api/sessions/" + session.auth_token
      }).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 404);
          start();
        }
      );
    }
  );
});

asyncTest("DELETE /api/sessions/:auth_token with valid auth_token returns 204", function(){
  expect(3);
  utils.generateSession(
    function(session){
      $.ajax({
        type: "DELETE",
        url: "/api/sessions/" + session.auth_token
      }).then(
        function(data, textStatus, jqXHR){ // success
          equal(jqXHR.status, 204);
          start();
        },
        function(){ // failure
          start();
        }
      );
    }
  );
});

asyncTest("DELETE /api/sessions/:auth_token with invalid auth_token returns 404", function(){
  expect(1);
  $.ajax({
    type: "DELETE",
    url: "/api/sessions/1"
  }).then(
    function(){ // success
      start();
    },
    function(data){ // failure
      equal(data.status, 404);
      start();
    }
  );
});
