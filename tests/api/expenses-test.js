import Ember from 'ember';
import startApp from '../helpers/start-app';
import utils from '../helpers/utils';

var App;

module("API: expenses resource", {
  setup: function() {
    App = startApp();
  },
  teardown: function() {
    Ember.run(App, 'destroy');
  }
});

asyncTest("POST /api/expenses with valid expense returns 200", function(){
  expect(5);
  utils.generateSession(
    function(session){
      $.ajax({
        type: "POST",
        url: "/api/expenses",
        data: {expense: {date: "2014-01-31", amount: "15"}},
        headers: {"AUTH_TOKEN": session.auth_token}
      }).then(
        function(data){ // success
          var expense = data.expense;
          ok(expense.id!=null);
          equal(expense.date, "2014-01-31");
          equal(expense.amount, "15.00");
          start();
        },
        function(){ // failure
          start();
        }
      );
    }
  );
});

asyncTest("POST /api/expenses with invalid AUTH_TOKEN returns 401", function(){
  expect(1);
  $.ajax({
    type: "POST",
    url: "/api/expenses",
    data: {expense: {date: "2014-01-31", amount: "15"}},
    headers: {"AUTH_TOKEN": "INVALID"}
  }).then(
    function(){ // success
      start();
    },
    function(data){ // failure
      equal(data.status, 401);
      start();
    }
  );
});

asyncTest("POST /api/expenses without AUTH_TOKEN returns 401", function(){
  expect(1);
  $.post("/api/expenses", {expense: {date: "2014-01-31", amount: "15"}}).then(
    function(){ // success
      start();
    },
    function(data){ // failure
      equal(data.status, 401);
      start();
    }
  );
});

asyncTest("POST /api/expenses with invalid expense returns 422", function(){
  expect(4);
  utils.generateSession(
    function(session){
      $.ajax({
        type: "POST",
        url: "/api/expenses",
        headers: {"AUTH_TOKEN": session.auth_token}
      }).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 422);
          var errors = {
            date: ["can't be blank"],
            amount: ["can't be blank"]
          };
          equal(data.responseText, JSON.stringify({errors: errors}));
          start();
        }
      );
    }
  );
});

asyncTest("POST /api/expenses cleans up invalid UTF-8 characters", function(){
  expect(3);
  utils.generateSession(
    function(session){
      var data = $.param({
        expense: {
          date: "2014-01-31",
          amount: "15",
          description: ""
        }
      });
      $.ajax({
        type: "POST",
        url: "/api/expenses",
        contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",
        data: (data + "Test\uD800"),
        headers: {"AUTH_TOKEN": session.auth_token}
      }).then(
        function(data){ // success
          var expense = data.expense;
          equal(expense.description, "Test");
          start();
        },
        function(data){ // failure
          start();
        }
      );
    }
  );
});

asyncTest("GET /api/expenses returns only current user expenses", function(){
  expect(8);
  utils.generateExpense(
    function(session1, expense1){
      utils.generateExpense(
        function(session2, expense2){
          $.ajax({
            url: "/api/expenses",
            headers: {"AUTH_TOKEN": session2.auth_token}
          }).then(
            function(data){ // success
              var expenses = data.expenses;
              equal(expenses.length, 1);
              equal(expenses[0].id, expense2.id);
              start();
            },
            function(){ // failure
              start();
            }
          );
        }
      );
    }
  );
});

asyncTest("GET /api/expenses with invalid AUTH_TOKEN returns 401", function(){
  expect(1);
  $.ajax({
    url: "/api/expenses",
    headers: {"AUTH_TOKEN": "INVALID"}
  }).then(
    function(){ // success
      start();
    },
    function(data){ // failure
      equal(data.status, 401);
      start();
    }
  );
});

asyncTest("GET /api/expenses without AUTH_TOKEN returns 401", function(){
  expect(1);
  $.get("/api/expenses").then(
    function(){ // success
      start();
    },
    function(data){ // failure
      equal(data.status, 401);
      start();
    }
  );
});

asyncTest("GET /api/expenses/:id with valid AUTH_TOKEN returns 404", function(){
  expect(4);
  utils.generateExpense(
    function(session, expense){
      $.ajax({
        url: ("/api/expenses/" + expense.id),
        headers: {"AUTH_TOKEN": session.auth_token}
      }).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 404);
          start();
        }
      );
    }
  );
});

asyncTest("GET /api/expenses/:id with invalid AUTH_TOKEN returns 401", function(){
  expect(4);
  utils.generateExpense(
    function(session, expense){
      $.ajax({
        url: ("/api/expenses/" + expense.id),
        headers: {"AUTH_TOKEN": "INVALID"}
      }).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 401);
          start();
        }
      );
    }
  );
});

asyncTest("GET /api/expenses/:id without AUTH_TOKEN returns 401", function(){
  expect(4);
  utils.generateExpense(
    function(session, expense){
      $.get("/api/expenses/" + expense.id).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 401);
          start();
        }
      );
    }
  );
});

asyncTest("PUT /api/expenses/:id with valid expense returns 204", function(){
  expect(4);
  utils.generateExpense(
    function(session, expense){
      $.ajax({
        type: "PUT",
        url: ("/api/expenses/" + expense.id),
        data: {expense: {date: "2014-01-31", amount: "30"}},
        headers: {"AUTH_TOKEN": session.auth_token}
      }).then(
        function(data, textStatus, jqXHR){ // success
          equal(jqXHR.status, 204);
          start();
        },
        function(){ // failure
          start();
        }
      );
    }
  );
});

asyncTest("PUT /api/expenses/:id with invalid expense returns 204", function(){
  expect(5);
  utils.generateExpense(
    function(session, expense){
      $.ajax({
        type: "PUT",
        url: ("/api/expenses/" + expense.id),
        data: {expense: {amount: "30"}},
        headers: {"AUTH_TOKEN": session.auth_token}
      }).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 422);
          var errors = {date: ["can't be blank"]};
          equal(data.responseText, JSON.stringify({errors: errors}));
          start();
        }
      );
    }
  );
});

asyncTest("PUT /api/expenses/:id with invalid AUTH_TOKEN returns 401", function(){
  expect(4);
  utils.generateExpense(
    function(session, expense){
      $.ajax({
        type: "PUT",
        url: ("/api/expenses/" + expense.id),
        data: {expense: {date: "2014-01-31", amount: "30"}},
        headers: {"AUTH_TOKEN": "INVALID"}
      }).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 401);
          start();
        }
      );
    }
  );
});

asyncTest("PUT /api/expenses/:id without AUTH_TOKEN returns 401", function(){
  expect(4);
  utils.generateExpense(
    function(session, expense){
      $.ajax({
        type: "PUT",
        url: ("/api/expenses/" + expense.id),
        data: {expense: {date: "2014-01-31", amount: "30"}},
      }).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 401);
          start();
        }
      );
    }
  );
});

asyncTest("DELETE /api/expenses/:id with valid expense returns 204", function(){
  expect(4);
  utils.generateExpense(
    function(session, expense){
      $.ajax({
        type: "DELETE",
        url: ("/api/expenses/" + expense.id),
        headers: {"AUTH_TOKEN": session.auth_token}
      }).then(
        function(data, textStatus, jqXHR){ // success
          equal(jqXHR.status, 204);
          start();
        },
        function(data){ // failure
          console.log("DELETE:", data);
          start();
        }
      );
    }
  );
});

asyncTest("DELETE /api/expenses/:id with invalid AUTH_TOKEN returns 401", function(){
  expect(4);
  utils.generateExpense(
    function(session, expense){
      $.ajax({
        type: "DELETE",
        url: ("/api/expenses/" + expense.id),
        headers: {"AUTH_TOKEN": "INVALID"}
      }).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 401);
          start();
        }
      );
    }
  );
});

asyncTest("DELETE /api/expenses/:id without AUTH_TOKEN returns 401", function(){
  expect(4);
  utils.generateExpense(
    function(session, expense){
      $.ajax({
        type: "DELETE",
        url: ("/api/expenses/" + expense.id),
      }).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 401);
          start();
        }
      );
    }
  );
});
