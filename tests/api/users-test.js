import Ember from 'ember';
import startApp from '../helpers/start-app';
import utils from '../helpers/utils';

var App;

module("API: users resource", {
  setup: function() {
    App = startApp();
  },
  teardown: function() {
    Ember.run(App, 'destroy');
  }
});

asyncTest("POST /api/users with valid email and password returns 200", function(){
  expect(3);
  var email = utils.generateEmail();
  $.post(
    "/api/users",
    {user: {email: email, password: "sponge"}}
  ).then(
    function(data){ // success
      ok(data.user.id!=null);
      equal(data.user.email, email);
      equal(data.user.password, null);
      start();
    },
    function(){ // failure
      start();
    }
  );
});

asyncTest("POST /api/users with email already taken returns 422", function(){
  expect(3);
  utils.generateUser(
    function(user){
      $.post(
        "/api/users",
        {user: {email: user.email, password: "sponge2"}}
      ).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 422);
          var errors = {email: ["is already taken"]};
          equal(data.responseText, JSON.stringify({errors: errors}));
          start();
        }
      );
    }
  );
});

asyncTest("POST /api/users with valid password but invalid email returns 422", function(){
  expect(2);
  $.post(
    "/api/users",
    {user: {email: "spongebob", password: "sponge"}}
  ).then(
    function(){ // success
      start();
    },
    function(data){ // failure
      equal(data.status, 422);
      var errors = {email: ["is not valid"]};
      equal(data.responseText, JSON.stringify({errors: errors}));
      start();
    }
  );
});

asyncTest("POST /api/users with valid email but no password returns 422", function(){
  expect(2);
  $.post(
    "/api/users",
    {user: {email: utils.generateEmail(), password: ""}}
  ).then(
    function(){ // success
      start();
    },
    function(data){ // failure
      equal(data.status, 422);
      var errors = {password: ["is mandatory"]};
      equal(data.responseText, JSON.stringify({errors: errors}));
      start();
    }
  );
});

asyncTest("POST /api/users with no data at all returns 422", function(){
  expect(2);
  $.post("/api/users").then(
    function(){ // success
      start();
    },
    function(data){ // failure
      equal(data.status, 422);
      var errors = {email: ["is not valid"], password: ["is mandatory"]};
      equal(data.responseText, JSON.stringify({errors: errors}));
      start();
    }
  );
});

asyncTest("GET /api/users returns 404", function(){
  expect(1);
  $.get("/api/users").then(
    function(){ // success
      start();
    },
    function(data){ // failure
      equal(data.status, 404);
      start();
    }
  );
});

asyncTest("GET /api/users/:id returns 404", function(){
  expect(2);
  utils.generateUser(
    function(user){
      $.get("/api/users/" + user.id).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 404);
          start();
        }
      );
    }
  );
});

asyncTest("PUT /api/users/:id returns 404", function(){
  expect(2);
  utils.generateUser(
    function(user){
      $.ajax({
        type: "PUT",
        url: "/api/users/" + user.id
      }).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 404);
          start();
        }
      );
    }
  );
});

asyncTest("DELETE /api/users/:id returns 404", function(){
  expect(2);
  utils.generateUser(
    function(user){
      $.ajax({
        type: "DELETE",
        url: "/api/users/" + user.id
      }).then(
        function(){ // success
          start();
        },
        function(data){ // failure
          equal(data.status, 404);
          start();
        }
      );
    }
  );
});
