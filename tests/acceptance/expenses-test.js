import Ember from 'ember';
import startApp from '../helpers/start-app';

var App;

module('Acceptance: Expenses', {
  setup: function() {
    App = startApp();
  },
  teardown: function() {
    Ember.run(App, 'destroy');
  }
});

test('visiting /expenses while not logged in redirects to /sign-in', function() {
  visit('/expenses');

  andThen(function() {
    equal(currentPath(), 'sign-in');
  });
});

test('visiting /expenses while logged in lists expenses', function() {
  expect(2);
  visit('/expenses');
  fillIn("#login-name", "top");
  fillIn("#login-pass", "top");
  click("input.btn-primary");

  andThen(
    function(){
      equal(currentPath(), 'expenses.index');
      equal(find("div.expense").length, 2, "Expected 2 expenses");
    }
  );
});

test('I can add a new expense, then delete it', function() {
  expect(2);
  visit('/expenses');
  fillIn("#login-name", "top");
  fillIn("#login-pass", "top");
  click(".btn-primary");

  andThen(
    function(){
      fillIn(".new-expense input[placeholder='Date']", "2014-01-30");
      fillIn(".new-expense input[placeholder='Amount']", "14.5");
      click(".new-expense .btn-primary");

      andThen(
        function(){
          equal(find("div.expense").length, 3, "Expected 3 expenses");
          click(".expense:nth-child(6) .btn-danger");

          andThen(
            function(){
              equal(find("div.expense").length, 2, "Expected 2 expenses");
            }
          );
        }
      );
    }
  );
});
