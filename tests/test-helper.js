import resolver from './helpers/resolver';
import {
  setResolver
} from 'ember-qunit';

setResolver(resolver);

document.write('<div id="ember-testing-container"><div id="ember-testing"></div></div>');

QUnit.config.urlConfig.push({ id: 'nocontainer', label: 'Hide container'});
var containerVisibility = QUnit.urlParams.nocontainer ? 'hidden' : 'visible';
document.getElementById('ember-testing-container').style.visibility = containerVisibility;

function generateEmail()
{
  var random = "-xxxxxxxx".replace(/[x]/g, function(){
    return (Math.random()*16|0).toString(16);
  });
  return "spongebob@bikini.com".replace(/@/, random + "@");
}
function generateUser(callback){
  $.post(
    "/api/users",
    {user: {email: generateEmail(), password: "sponge"}}
  ).then(
    function(data){ // success
      var user = data.user;
      ok(user.id!=null);
      callback(data.user);
    },
    function(){ // failure
      start();
    }
  );
}
function generateSession(callback){
  generateUser(
    function(user){
      $.post(
        "/api/sessions",
        {username: user.email, password: "sponge"}
      ).then(
        function(data_session){ // success
          var session = data_session.session;
          ok(session.auth_token!=null);
          callback(session);
        },
        function(){ // failure
          start();
        }
      );
    }
  );
}
