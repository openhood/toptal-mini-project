export default {
  generateEmail: function()
  {
    var random = "-xxxxxxxx".replace(/[x]/g, function(){
      return (Math.random()*16|0).toString(16);
    });
    return "spongebob@bikini.com".replace(/@/, random + "@");
  },
  generateUser: function(callback){ // generate 1 assertion
    $.post(
      "/api/users",
      {user: {email: this.generateEmail(), password: "sponge"}}
    ).then(
      function(data){ // success
        var user = data.user;
        ok(user.id!=null);
        callback(data.user);
      },
      function(){ // failure
        start();
      }
    );
  },
  generateSession: function(callback){ // generate 2 assertions
    this.generateUser(
      function(user){
        $.post(
          "/api/sessions",
          {username: user.email, password: "sponge"}
        ).then(
          function(data_session){ // success
            var session = data_session.session;
            ok(session.auth_token!=null);
            callback(session);
          },
          function(){ // failure
            start();
          }
        );
      }
    );
  },
  generateExpense: function(callback){ // generate 3 assertions
    this.generateSession(
      function(session){
        $.ajax({
          type: "POST",
          url: "/api/expenses",
          data: {expense: {date: "2014-01-31", amount: "15"}},
          headers: {"AUTH_TOKEN": session.auth_token}
        }).then(
          function(data_expense){ // success
            var expense = data_expense.expense;
            ok(expense.id!=null);
            callback(session, expense);
          },
          function(){ // failure
            start();
          }
        );
      }
    );
  }
};
